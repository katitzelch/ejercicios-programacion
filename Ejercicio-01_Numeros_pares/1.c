#include <stdio.h>

int main()
{
    int numero;

    printf( "\n   " );

    for ( numero = 0 ; numero <= 100 ; numero += 2 )
    {
        printf( "%d ", numero );
    }

    getchar(); /* Pausa */

    return 0;
}